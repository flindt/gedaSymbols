Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-04-16T07:03:28+02:00

====== runSimulation ======
Created Saturday 16 April 2016

== Create netlist ==
* gnetlist -g spice-sdb -o output.net basic.sch
Mostly you want to sort the netlist.
* gnetlist -g spice-sdb -O sort_mode -o output.net basic.sch

== Run simulation ==
* dc i1 1u 100u 1u
Notice that the changed parameter 'i1' is non-capital, even though the circiut name is capital.

== Plotting ==
* plot v(output)
* plot v(1)

== Running simulation with command file ==
Create a command file with
.control
dc i1 -100u 100u 1u
plot v(output)
.endc

This can be included in the schematic file with a SPICE directive, and using the file attribute.

== Subcircuits ==
When working with subcircuits (ie mosfets), you must put the name of the model in the "device" attribute as well as in the "model-name" attribute.
For libraries with "_L0" and "_L1"  postfixes it seems only the former works.

