*model for MAX9934F
*version 0: 10/2012
*version 1: 3/2013 BW
**************************************************************************************
.subckt MAX9934F RS+ RS- GND OUT CS VCC
R+ RS+ ib+ 1g
R- RS- ib- 1g
Vi+ ib+ GND 0
Vi- ib- GND 0
Fi+ RS+ GND VALUE={-I(Vi+)+0.1n}
Fi- RS- GND VALUE={-I(Vi-)+0.1n}
Rs+ RS+ rsp 100
Cs+ rsp RS- 320n
Fo GND OUT VALUE={((V(rsp)-V(RS-))*V(hp)+(1u+V(cmr)+V(psr)-0.948714u)*V(vcchp))*5m*V(hp)*V(vcchp)}
Esc sc GND VALUE={(V(hp)-1)}
Ell ll GND VALUE={0.15*V(hp)}
Ecmr cmr GND VALUE={(V(RS+)+V(RS-))/2*3.16e-7}
Epsr psr GND VALUE={V(VCC)*1u}
Elim lim lim1 VCC GND 1
Elim1 lim1 GND VALUE={-0.1-0.4*V(sc)}
Dlim OUT lim DX1
Dlima ll OUT DX1
X_cs VCC GND CS hp shutdown
X_vcchp VCC GND VCC vcchp shutdown
.MODEL DX1  D(n=0.001)
ICC VCC GND 113.4u
.ends
**************************************************************************************
.SUBCKT shutdown 10 18 91 cs
E20 20 18 10 18 0.5
Ecs cs 18 VALUE={V(96)-V(20)}
*SHUTDOWN
RSHIN 91 20 500K
ESH3 220 20 91 18 1
RSHA 220 221 1K
CSHA 221 20 32P
ESH2 92 20 221 20 1
RSH1 92 93 100
VSH1 93 94 0V
EHYS 95 94 POLY(2) 10 18 96 20 0 0 0 0 0.1
RBL 94 20 10K
ESH1 95 20 10 18 0.35
****
FSH1 20 96 Vsh1 1
CSH1 96 20 10P
DSH2 20 96 DA
DSH1 96 97 DA
VSH2 97 20 1V
RSH2 96 20 100K
.MODEL DA D(N=0.1M)
.ends
**************************************************************************************



* Copyright (c) 2003-2013 Maxim Integrated Products.  All Rights Reserved.
